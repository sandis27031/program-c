#include <stdio.h>

int main()
{
    float bill[10];                         //tipe data yang digunakan//
    int i;

    for(i=0;i<10;i++){                      //pengulangan for untung mengulang program yang sama//
        printf("Masukkan bilangan ");       //meminta agar user menginputkan bilangan//
        scanf("%f", &bill[i]);              //melakukan scan pada input yang diberikan user lalu meyimpannya pada indeks array//
    }
    
    for(i=0;i<10;i++){                      //menggunakan pengulangan for untuk mengulang program//
      if ((int)(bill[i] * 10) % 2 != 0){    //rumus untuk mencari bilangan ganjil pada dari sebuah data bertipe float//
            printf("%.1f adalah bilangan ganjil\n" ,bill[i]);       //mencetak output berupa data bilangan ganjil bertipe float//
        }  
    }
    

    return 0;
}