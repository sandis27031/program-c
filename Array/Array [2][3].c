
#include <stdio.h>

void printArray(int a[][3]); //prototype fungsi

int main(void){
    //inisialisasi array 1,array 2, array 3
    int array1[ 2 ][ 3 ] = { { 1, 2, 3 }, { 4, 5, 6 } };
    int array2[ 2 ][ 3 ] = { 1 ,2, 4, 5 };
    int array3[ 2 ][ 3 ] = { { 1, 2 }, { 4 } };
    
    puts( "Nilai setiap baris array 1:");
    printArray( array1 );
    puts( "Nilai setiap baris array 2:" );
    printArray( array2 );
    puts( "Nilai setiap baris array 3:" );
    printArray( array3 );
    
} //end main

//implementasi fungsi untuk mencetak
//array 2 kolom 3 baris

void printArray( int a[][ 3 ] ) {
    int i; //counter kolom
    int j; //counter baris
     
    //pengulangan baris
    for(i=0;i<=1;i++){
    //output nilai kolom
        for(j=0;j<=2;j++){
            printf("%d ",a[i][j]);
        }
    printf("\n");
    }
    
} //end function printArray