#include <stdio.h>
#include <stdlib.h>
#define MATSIZE 15
void isi_matriksA(int MatX[][MATSIZE], int barA, int kolA);
void isi_matriksB(int MatX[][MATSIZE], int barB, int kolB);
void cetak_matriksA(int MatX[][MATSIZE], int barA, int kolA);
void cetak_matriksB(int MatX[][MATSIZE], int barB, int kolB);
void cetak_matriks(int MatX[][MATSIZE], int barA, int kolA, int barB, int kolB);
void jumlah_matriks(int MatX[][MATSIZE],int MatY[][MATSIZE],int barA,int kolA,int barB,int kolB);
void kali_matriks(int MatX[][MATSIZE],int MatY[][MATSIZE],int barA,int kolA,int barB,int kolB);

void main() {
    int MatA[MATSIZE][MATSIZE];
    int MatB[MATSIZE][MATSIZE];
    int barA, kolA,barB, kolB;
    
    printf("Jumlah baris Matriks A : ");
    scanf("%d",&barA);
    printf("Jumlah kolom Matriks A : ");
    scanf("%d",&kolA);
    isi_matriksA(MatA,barA,kolA);
    putchar('\n');
    printf("Isi Matriks A: \n");
    cetak_matriksA(MatA,barA,kolA);
    putchar('\n');
    printf("Jumlah baris Matriks B : ");
    scanf("%d",&barB);
    printf("Jumlah kolom Matriks B : ");
    scanf("%d",&kolB);
    isi_matriksB(MatB,barB,kolB);
    putchar('\n');
    printf("Isi Matriks B: \n");
    cetak_matriksB(MatB,barB,kolB);
    putchar('\n');
    jumlah_matriks(MatA, MatB, barA, kolA, barB, kolB);
    putchar('\n');
    kali_matriks(MatA, MatB, barA, kolA, barB, kolB);
}

void isi_matriksA(int MatX[][MATSIZE], int barA, int kolA){
    int i,j;
    for(i=0;i<barA;i++) {
        for (j=0;j<kolA;j++){
            printf("Elemen Matriks [%d, %d] = ",i,j);
            scanf("%d",&MatX[i][j]);
        }
    }
}

void isi_matriksB(int MatX[][MATSIZE], int barB, int kolB){
    int i,j;
    for(i=0;i<barB;i++) {
        for (j=0;j<kolB;j++){
            printf("Elemen Matriks [%d, %d] = ",i,j);
            scanf("%d",&MatX[i][j]);
        }
    }
}

void cetak_matriksA(int MatX[][MATSIZE], int barA, int kolA) {
    int i,j;
    for(i=0;i<barA;i++){
        for (j=0;j<kolA;j++){
            printf("%4d",MatX[i][j]);
        }
        printf("\n");
    }
}

void cetak_matriksB(int MatX[][MATSIZE], int barB, int kolB) {
    int i,j;
    for(i=0;i<barB;i++){
        for (j=0;j<kolB;j++){
            printf("%4d",MatX[i][j]);
        }
        printf("\n");
    }
}

void cetak_matriks(int MatX[][MATSIZE], int barA, int kolA, int barB, int kolB) {
    int i,j;
    for(i=0;i<barA;i++){
        for (j=0;j<kolB;j++){
            printf("%4d",MatX[i][j]);
        }
        printf("\n");
    }
}

void jumlah_matriks(int MatX[][MATSIZE],int MatY[][MATSIZE],int barA,int kolA,int barB,int kolB) {
    int MatZ[MATSIZE][MATSIZE];
    int i,j;
    
    if((barA == barB) && (kolA == kolB)){
        for(i=0;i<barA;i++){
            for (j=0;j<kolB;j++){
                MatZ[i][j]=MatX[i][j]+MatY[i][j];
            }
        }
        printf("Isi Matriks A+B: \n");
        cetak_matriks(MatZ,barA,kolA,barB,kolB);
    }    
    else{
        printf("Tidak dapat dijumlahkan \n");
    }
}

void kali_matriks(int MatX[][MATSIZE],int MatY[][MATSIZE],int barA,int kolA,int barB,int kolB) {
    int MatZ[MATSIZE][MATSIZE];
    int i,j,l,jumlah = 0;
            
    if(kolA == barB){
        for(i=0;i<barA;i++){
            for(j=0;j<kolB;j++){
                for(l=0;l<barB;l++){
                    jumlah = jumlah + MatX[i][l] * MatY[l][j];
                }
                MatZ[i][j] = jumlah;
                jumlah = 0;
            }
        }
        printf("Isi Matriks A*B: \n");
        cetak_matriks(MatZ,barA,kolA,barB,kolB);
    }else{
        printf("Matriks tidak dapat dikalikan");
        exit(0);
    }
}