#include <stdio.h>
#include <stdlib.h>

// variabel global
int a,b;
char pilihan,status;

// Deklarasi fungsi/prosedur
void menu();
void inputData();
void jumlah();
void kurang();
void kali();
void bagi();

int main(){
    
    // panggil prosedur menu()
    menu();
    printf("Masukkan pilihan (1/2/3/4) : ");
    scanf("%c", &pilihan);
    
    switch(pilihan){
        case '1': {
                    //panggil prosedur jumlah()
                    jumlah();
                    break;
                }
        case '2': {
                    //panggil prosedur kurang()
                    kurang();
                    break;
                }
        case '3' : {
                    //panggil prosedur kali()
                    kali();
                    break;
                }
         case '4' : {
                    //panggil prosedur bagi()
                    bagi();
                    break;
                }
        default: printf("Masukkan salah. \n");
    }
    
    scanf("%c",&status);
    if (status == 'y'){
        menu();
    }
    
    return 0;
}

// Implementasi Prosedur
void menu(){
    char pilihan;
    printf("===============Menu===============\n"
    "1. Penjumlahan\n"
    "2. Pengurangan\n"
    "3. Perkalian\n"
    "4. Pembagian\n");
}

void inputData(){
    printf("Nilai A: ");
    scanf("%d",&a);
    printf("Nilai B: ");
    scanf("%d",&b);
}

void jumlah(){
    // memanggil prosedur inputData() dari dalam prosedur jumlah()
    inputData();
    printf("%d + %d = %d\n", a, b, a+b);
}

void kurang(){
    // memanggil prosedur inputData() dari dalam prosedur jumlah()
    inputData();
    printf("%d - %d = %d\n", a, b, a-b);
}

void kali(){
    // memanggil prosedur inputData() dari dalam prosedur jumlah()
    inputData();
    printf("%d * %d = %d\n", a, b, a*b);
}

void bagi(){
    // memanggil prosedur inputData() dari dalam prosedur jumlah()
    inputData();
    printf("%d / %d = %d\n", a, b, a/b);
}