#include <stdio.h>

int main(){
	//Deklarasi variabel
	int tab[9] = {1, 4, 5, 2, 3, 7, 8, 9, 6}; //variabel array yang telah didefinisikan
	int i;
	int temp;
	int tukar;
	
	//pengulangan setiap pass
	do{
		tukar = 0;
		//pengulangan untuk mengecek apakah ada pengulangan
		for(i=0;i<(9-1);i++){
			printf("Langkah %d, status tukar %d: ", i, tukar);
			if (tab[i]>tab[i+1]){
				
				temp = tab[i];
				tab[i] = tab[i+1];
				tab[i+1] = temp;
				printf("Terjadi Pertukaran\n");
				tukar = 1;
			}else{
				printf("Tidak ada pertukaran\n");
			}
		}
		printf("Status tukar akhir = %d \n", tukar);
		
	}while (tukar == 1);
	
	for(i=0;i<9;i++){
		printf("%d ", tab[i]);
	}
	
	return 0;
}
