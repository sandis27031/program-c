#include <math.h>
#include <stdio.h>
#include "point.h"

POINT makePoint(int XX, int YY){
	POINT P;
	
	P.X = XX;
	P.Y = YY;
	
	return P;
}

void printPoint(POINT P){
	
	printf("Point = (%d, %d)\n", P.X, P.Y);
	
}

double jarakPoint(POINT P1, POINT P2){
	double jarak;
	
	jarak = sqrt(pow((P2.X-P1.X),2)+(pow((P2.Y-P1.Y),2)));
	
	return jarak;
}

int kuadranPoint(POINT P){
	
	if (P.X > 0 && P.Y > 0){
	return 1;
	} else if (P.X < 0 && P.Y > 0){
	return 2;
	} else if (P.X < 0 && P.Y < 0){
	return 3;
	} else {
	return 4;
	}
}
