#ifndef point_h
#define point_h

typedef struct{
	int X;
	int Y;
}POINT;

POINT makePOINT(int x, int y);
void printPoint(POINT P);
double jarakPoint(POINT P1, POINT P2);
int kuadranPoint(POINT P);

#endif

