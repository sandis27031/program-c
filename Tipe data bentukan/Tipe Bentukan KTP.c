#include <stdio.h>
#include <conio.h>
#include <string.h>

typedef struct {
	char NIK[50];
	char tmpt_tinggal[20];
	char namadpn[8]; 
	char namatgh[8];
	char namablk[8];
	char tempat_lahir[50];
	char tanggal_lahir[20];
	char kelamin[10];
	char gol_darah[10];
	char alamat[50];
	char rt_rw[10];
	char kel_des[50];
	char kec[20];
	char kab[20];
	char agama[20];
	char pernikahan[20];
	char pekerjaan[50];
	char warga_neg[20];
	char exp[10];
}identitas;
   

void main(){
  int i;
  identitas KTP[1];
  
  printf("==========INPUT DATA KTP==========\n");
  
  for(i=0; i<1; i++){
  	printf("NIK\t\t\t\t: "); scanf("%s", KTP[i].NIK);
	printf("Nama Depan\t\t\t: "); scanf("%s", KTP[i].namadpn);
	printf("Nama Tengah(- jika tidak ada)\t: "); scanf("%s", KTP[i].namatgh);
	printf("Nama Akhir(- jika tidak ada)\t: "); scanf("%s", KTP[i].namablk);
	printf("Tempat Lahir\t\t\t: "); scanf("%s", KTP[i].tempat_lahir);
	printf("Tanggal lahir dd/mm/yy\t\t: "); scanf("%s", KTP[i].tanggal_lahir);
	printf("Jenis Kelamin (Pria/Wanita)\t: "); scanf("%s", KTP[i].kelamin);
	printf("Golongan darah\t\t\t: "); scanf("%s", KTP[i].gol_darah);
	printf("Alamat\t\t\t\t: "); scanf("%s", KTP[i].alamat);
	printf("RT/RW\t\t\t\t: "); scanf("%s", KTP[i].rt_rw);
	printf("Kelurahan/Desa\t\t\t: "); scanf("%s", KTP[i].kel_des);
	printf("Kecamatan\t\t\t: "); scanf("%s", KTP[i].kec);
	printf("Kabupaten/Kota\t\t\t: "); scanf("%s", KTP[i].kab);
	printf("Agama\t\t\t\t: "); scanf("%s", KTP[i].agama);
	printf("Status Pernikahan\t\t: "); scanf("%s", KTP[i].pernikahan);
	printf("Pekerjaan\t\t	: "); scanf("%s", KTP[i].pekerjaan);
	printf("Kewarganegaraan\t\t\t: "); scanf("%s", KTP[i].warga_neg);
	printf("Berlaku sampai dd/mm/yy\t\t: "); scanf("%s", KTP[i].exp);
  }
  
  	for(i=0; i<1; i++){
  		printf("-------------------------------------------------------------\n");
  		printf("KARTU TANDA PENDUDUK\n");
  	
		printf("NIK\t\t\t\t: %s\n",KTP[i].NIK);
  		printf("Nama\t\t\t\t: %s %s %s\n", KTP[i].namadpn, KTP[i].namatgh, KTP[i].namablk);
  		printf("Tempat/Tgl Lahir\t\t: %s, %s\n", KTP[i].tempat_lahir, KTP[i].tanggal_lahir);
  		printf("Jenis Kelamin\t\t\t: %s\n", KTP[i].kelamin);
  		printf("Alamat\t\t\t\t: %s\n",KTP[i].alamat);
  		printf("\t\tRT\t\t: %s\n",KTP[i].rt_rw);
  		printf("\t\tKelurahan/Desa	: %s\n",KTP[i].kel_des);
  		printf("\t\tKecamatan\t: %s\n",KTP[i].kec);
  		printf("\t\tKabupaten/Kota	: %s\n",KTP[i].kab);
  		printf("Agama\t\t\t\t: %s\n",KTP[i].agama);
		printf("Status Pernikahan\t\t: %s\n",KTP[i].pernikahan);
		printf("Pekerjaan\t\t\t: %s\n",KTP[i].pekerjaan);
		printf("Kewarganegaraan\t\t\t: %s\n",KTP[i].warga_neg);
		printf("Berlaku sampai dd/mm/yy\t\t: %s\n",KTP[i].exp);
			
  }
  getch();
}
