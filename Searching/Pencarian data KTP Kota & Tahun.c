#include <stdio.h>
#include <string.h>


typedef struct {
    char nama[10];
    char kota [10];
    int tahun_lahir;
    int umur;
}KTP;

void biodata(KTP* data){
    int i, ketemu;
    i=0;
    while(i < 5){
        if(strcmp(data[i].kota, "Lembang") == 0){
            printf("Nama                            : %s\n", data[i].nama);
            printf("Tahun Lahir                     : %d\n", data[i].tahun_lahir);
            printf("Kota                            : %s\n", data[i].kota);
            printf("Data ditemukan pada indeks ke   : %d\n\n", i);
        }
        i++;
    }
}

void umur(KTP* data){
    int i, ketemu;
    i=0;
    while(i < 5){
        if(data[i].umur > 20){
            printf("Nama                            : %s\n", data[i].nama);
            printf("Tahun Lahir                     : %d\n", data[i].tahun_lahir);
            printf("Kota                            : %s\n", data[i].kota);
            printf("Umur                            : %d\n\n", data[i].umur);
        }
        i++;
    }
}
int main(){
    KTP data[5];
    char status;
    char pilihan;
    
    strcpy(data[0].nama, "Sandi");
    strcpy(data[0].kota, "Lembang");
    data[0].tahun_lahir = 2002;
    data[0].umur = 2020 - 2002;
    
    strcpy(data[1].nama, "Yusup");
    strcpy(data[1].kota, "Medan");
    data[1].tahun_lahir = 1999;
    data[1].umur = 2020 - 1999;
    
    strcpy(data[2].nama, "Sinaga");;    
    strcpy(data[2].kota, "Bandung");
    data[2].tahun_lahir = 1986;
    data[2].umur = 2020 - 1986;
    
    strcpy(data[3].nama, "Megumin");
    strcpy(data[3].kota, "Lembang");
    data[3].tahun_lahir = 2002;
    data[3].umur = 2020 - 2002;
    
    strcpy(data[4].nama, "Kazuma");
    strcpy(data[4].kota, "Lembang");
    data[4].tahun_lahir = 1999;
    data[4].umur = 2020 - 1999;
    
    printf("\t\tBerikut ini adalah data dari warga yang tinggal di Lembang\n");
    biodata(data);
    printf("\t\tBerikut ini adalah data dari warga berumur diatas 20 tahun\n");
    umur(data);

    return 1;
}